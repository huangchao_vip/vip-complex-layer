import license from 'rollup-plugin-license'
import string from 'rollup-plugin-string'
import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'

import babel from 'rollup-plugin-babel'
import uglify from 'rollup-plugin-uglify'

const external = {
  // 'md5': 'md5',
}

const config = {
  sourcemap: true,
  external: [].concat(Object.keys(external), []),
  globals: Object.assign({}, external, {}),
  plugins: [
    string({
      include: 'src/**/*.tmpl',
    }),
    resolve({ jsnext: true }),
    commonjs(),
    babel({
      exclude: 'node_modules/**',
      runtimeHelpers: true,
    }),
    uglify(),
    license({
      banner: `Bundle of <%= pkg.name %>
      Generated: <%= moment().format('YYYY-MM-DD') %>
      Version: <%= pkg.version %>
      author: <%= pkg.author %>
      gitUrl: <%= pkg.repository.url %>`,
    }),
  ],
}

export default [
  Object.assign({
    input: 'src/index.js',
    output: {
      name: 'Cock',
      file: 'dist/cock.js',
      format: 'iife',
    },
  }, config),
  Object.assign({
    input: 'src/layer/layer.js',
    output: {
      name: 'layer',
      file: 'dist/layer.js',
      format: 'iife',
    },
  }, config)
]
