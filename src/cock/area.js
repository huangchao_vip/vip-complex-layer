import Cock from '../base/cock'
import tpl from './view/area.tmpl'
import data from './data/area_cn'
import cnLang from './lang/cn'

var
  auto = {
    name: 'area',
    tpl: tpl,
    data: data,
    // emp: { // 自定义class
    //   "item-hot": ["050100", "061300", "050500", "230300"],
    //   "item-bold": ["010000", "070100", "030000", "070200"],
    // },
    tip: '请选择工作地点',
    //multi: 1,
    ratio: 3, // 二级菜单长宽比率
    parent: false, // 这是父级可选
    mui: {},
  },
  i, key;

auto.data.lang = cnLang;


auto.data.type = {};
auto.data.all = [];
for (i in auto.data.raw) {
  key = parseInt(i.match(/\d{2}/), 10);
  if (/0{4}$/.test(i)) {
    auto.data.type[i] = auto.data.raw[i];
    auto.data.all[key] = [i, []];
  } else {
    auto.data.all[key][1].push(i);
  }
}


function Area(option, callback) {
  var data = $.extend({}, auto, option)

  if (option.allowed) {

    var allowedArr = option.allowed.toString().split(',');
    var arrParent = [];

    $.map(allowedArr, function (item) {
      if(item.substring(2) === '0000') {
        arrParent.push(item)
      }
    })

    $.map(arrParent, function (item) {
      var headParentId = item.substring(0,2)
      $.map(static_data.city, function (itemChilren) {
        var childrenId = itemChilren.code.substring(0,2)
        if(headParentId === childrenId) {
          allowedArr.push(itemChilren.code)
        }
      })
    })
    data.allowed = allowedArr;
    data.data.allowed = allowedArr;
  }

  Cock.run(data, callback);
}


Area.skin = Cock.skin;
Area.data = auto.data;

export default Area
