let all = []
let showNew = []
let raw = {}
let type = {}

// $.map(static_options.positions, list => {
//   let allList = []
//   allList[0] = list.code
//   allList[1] = []
//   type[list.code] = list.value
//   $.map(list.sublist, item => {
//     allList[1].push(item.code)
//     raw[item.code] = item.value
//   })
//   all.push(allList)
// })

// console.log(static_data)

$.map(static_data.department, (item) => {
  let allList = []
  allList[0] = item.code
  allList[1] = []
  type[item.code] = item.name
  if(item.showNew) {
    showNew.push(item.code)
  }
  $.map(static_data.position, (list) => {
    if(list.parent_id === item.id) {
      allList[1].push(list.code)
    }
    if(list.showNew) {
      showNew.push(item.code)
    }
    raw[list.code] = list.name
  })
  all.push(allList)
})

module.exports = {
  all,
  raw,
  type,
  showNew,
  // hot: [["酒店", ["1102", "1115", "1310"]], ["餐饮", ["1402", "1406", "1704"]], ["娱乐", ["2405", "1502", "1508"]], ["民宿/公寓", ["3001", "3004", "3006"]], ["景区/乐园", ["2901", "2902", "2904"]], ["航空/高铁", ["3101", "3102", "3104"]]],
  hot: [['高级管理',['1003','1921','0101','0901']],
    ['中层管理',['1004','1102','1302','1402']],
    ['基层管理',['1113','1304','1403','2303']],
    ['一线员工',['1014','1115','1310','1417']]],
}
