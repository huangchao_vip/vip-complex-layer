import Cock from '../base/cock'
import tpl from './view/post.tmpl'
import data from './data/post_cn'
import cnLang from './lang/cn'

var
  auto = {
    name: 'post',
    tpl: tpl,
    data: data,
    emp: { // 自定义class
      "item-new": data.showNew,
    },
    tip: '请选择职位',
    //multi: 1,
    ratio: 12,
    // parent: true, // 这是父级可选
    mui: {},
  };

auto.data.lang = cnLang;

// console.log(auto)

function Post(option, callback) {
  Cock.run($.extend({}, auto, option), callback);
}


Post.skin = Cock.skin;
Post.data = auto.data;

export default Post
